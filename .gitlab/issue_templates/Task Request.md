# Summary
(Summarize your idea concisely)

# Behavior
Bellow you will describe the current and the desired behavior

## Current
(Describe the current behavior, being the most descriptive as possible. Ex: When stone cubes are hit, no sound are emitted)

## Desired
(Describe your desired behavior, being the most descriptive as possible. Ex: On hit stone cubes, the [sound](https://freesound.org/people/nextmaking/sounds/86004/) should be emitted, on every hit)
